package com.example.foodpanda;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.loader.content.CursorLoader;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.auth0.android.jwt.Claim;
import com.auth0.android.jwt.JWT;
import com.example.foodpanda.ApiClient.TokenUtils;
import com.example.foodpanda.activities.DetailActivity;
import com.example.foodpanda.activities.LoginActivity;
import com.example.foodpanda.adapters.DrawerListAdapter;
import com.example.foodpanda.fragments.AddArticleFragment;
import com.example.foodpanda.fragments.ArticlesFragment;
import com.example.foodpanda.fragments.CommentListFragment;
import com.example.foodpanda.fragments.DiscountFragment;
import com.example.foodpanda.fragments.EditArticleFragment;
import com.example.foodpanda.fragments.MyFragment;
import com.example.foodpanda.fragments.OrdersFragment;
import com.example.foodpanda.fragments.ProfileFragment;
import com.example.foodpanda.fragments.UsersListFragment;
import com.example.foodpanda.tools.FragmentTransition;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import model.NavItem;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private RelativeLayout mDrawerPane;
    private CharSequence mTitle;
    private ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
    public static  final int READ_EXTERNAL_STORAGE = 1;
    private  static final int IMAGE = 100;
    Bitmap bitmap;
    ImageView imageView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prepareMenu(mNavItems);

        mTitle = getTitle();
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mDrawerList = findViewById(R.id.navList);

        mDrawerPane = findViewById(R.id.drawerPane);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);


        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerList.setAdapter(adapter);

        RelativeLayout profileLayout = findViewById(R.id.profileBox);

        TextView user = findViewById(R.id.userName);

        SharedPreferences preferences =  getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
        String retrivedToken  = preferences.getString("TOKEN",null);
        JWT jwt = new JWT(retrivedToken);

        Claim username = jwt.getClaim("sub");

        user.setText(username.asString());


        profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(ProfileFragment.newInstance(), MainActivity.this, false);
                setTitle("Profile");
                mDrawerLayout.closeDrawer(mDrawerPane);
            }
        });

        // Specificiramo da kada se drawer zatvori prikazujemo jednu ikonu
        // kada se drawer otvori drugu. Za to je potrebo da ispranvo povezemo
        // Toolbar i ActionBar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }

        /*
         * drawer-u specificiramo za koju referencu toolbar-a da se veze
         * Specificiramo sta ce da pise unutar Toolbar-a kada se drawer otvori/zatvori
         * i specificiramo sta ce da se desava kada se drawer otvori/zatvori.
         * */
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolbar,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle("foodpanda");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        // Izborom na neki element iz liste, pokrecemo akciju
        if (savedInstanceState == null) {
            String position = getIntent().getStringExtra("position");
            if(position != null){
                selectItemFromDrawer(Integer.valueOf(position));
            }else {
                selectItemFromDrawer(0);
            }
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    private void prepareMenu(ArrayList<NavItem> mNavItems ){
        String role = TokenUtils.getRole(MainActivity.this);


        if(role.equals("KUPAC")){
            mNavItems.add(new NavItem(getString(R.string.home), getString(R.string.home_long), R.drawable.ic_action_home));
            mNavItems.add(new NavItem(getString(R.string.reviews), getString(R.string.reviews_long), R.drawable.ic_action_reviews));

        }
        else if(role.equals("PRODAVAC")){
            mNavItems.add(new NavItem("Articles", getString(R.string.home_long), R.drawable.ic_action_home));
            mNavItems.add(new NavItem(getString(R.string.discounts), getString(R.string.discount_long), R.drawable.ic_action_discount));
            mNavItems.add(new NavItem(getString(R.string.new_article), getString(R.string.long_new_article), R.drawable.ic_add_article));
            mNavItems.add(new NavItem("Reviews", "Reviews", R.drawable.ic_action_reviews));
        }
        else{
            mNavItems.add(new NavItem(getString(R.string.all_users), getString(R.string.long_all_users), R.drawable.ic_all_users));

        }
        mNavItems.add(new NavItem(getString(R.string.logout), getString(R.string.long_logut), R.drawable.ic_logout));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItemFromDrawer(position);
        }
    }

    private void selectItemFromDrawer(int position) {
        String role = TokenUtils.getRole(MainActivity.this);
        if(role.equals("KUPAC")){
            if(position == 0){
                FragmentTransition.to(MyFragment.newInstance(), this, false);
            }else if(position == 1){
                FragmentTransition.to(OrdersFragment.newInstance(), this, false);
            }else if(position == 2){
                SharedPreferences preferences = getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
                preferences.edit().clear().apply();

                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }else{
                Log.e("DRAWER", "Nesto van opsega!");
            }
        }
        else if(role.equals("PRODAVAC")){
            if(position == 0){
                FragmentTransition.to(ArticlesFragment.newInstance(), this, false);
            }else if(position == 1){
                FragmentTransition.to(DiscountFragment.newInstance(), this, false);
            }else if(position == 2) {
                FragmentTransition.to(AddArticleFragment.newInstance(), this, false);
            }else if(position == 3){
                FragmentTransition.to(CommentListFragment.newInstance(), this, false);
            }else if(position == 4){
                SharedPreferences preferences = getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
                preferences.edit().clear().apply();

                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }else{
                Log.e("DRAWER", "Nesto van opsega!");
            }

        }else{
            if(position == 0){
                FragmentTransition.to(UsersListFragment.newInstance(), this, false);
            }else if(position == 1){
                SharedPreferences preferences = getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
                preferences.edit().clear().apply();

                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }

        }
        mDrawerList.setItemChecked(position, true);

        setTitle(mNavItems.get(position).getmTitle());

        mDrawerLayout.closeDrawer(mDrawerPane);


    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    public void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMAGE);
        imageView = findViewById(R.id.imageView);
    }

    public void updateImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMAGE);
        imageView = findViewById(R.id.imageView_articleEdit);
    }

    public String convertToString()
    {
        if(bitmap != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, byteArrayOutputStream);
            byte[] imgByte = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(imgByte, Base64.DEFAULT);
        }
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== IMAGE && resultCode==RESULT_OK && data!=null)
        {
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(MainActivity.this,"There is no back action",Toast.LENGTH_LONG).show();
        return;
    }



}