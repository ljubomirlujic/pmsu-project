package com.example.foodpanda.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.R;
import com.example.foodpanda.adapters.ArticleAdapter;
import com.example.foodpanda.fragments.ArticlesFragment;
import com.example.foodpanda.service.ArticleApiService;
import com.example.foodpanda.service.CustomerApiService;
import com.example.foodpanda.service.SellerApiService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;

import model.Article;
import model.Customer;
import model.Seller;
import model.User;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {


    static final String TAG = RegisterActivity.class.getSimpleName();
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);

        TextView adress = findViewById(R.id.adress_reg);
        final EditText nameEt = (EditText) findViewById(R.id.name_reg);
        final EditText surnameEt = (EditText) findViewById(R.id.surname_reg);
        final EditText usernameEt = (EditText) findViewById(R.id.username_reg);
        final EditText passwordEt = (EditText) findViewById(R.id.password_reg);
        final EditText adressEt = (EditText) findViewById(R.id.adress_reg);

        EditText emailEt = findViewById(R.id.email_reg);
        EditText companyNameEt = findViewById(R.id.name_company_reg);

        emailEt.setEnabled(false);
        companyNameEt.setEnabled(false);
        CheckBox checkBox = (CheckBox) findViewById(R.id.checkbox_reg);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    emailEt.setEnabled(true);
                    companyNameEt.setEnabled(true);
                }
                if(!isChecked){
                    emailEt.setEnabled(false);
                    companyNameEt.setEnabled(false);
                }
            }
        });

        Button button = (Button) findViewById(R.id.registerBtn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEt.getText().toString().trim();
                String surname = surnameEt.getText().toString().trim();
                String username = usernameEt.getText().toString().trim();
                String pasword = passwordEt.getText().toString().trim();
                String adress = adressEt.getText().toString().trim();

                if(!checkBox.isChecked()) {
                    if(validCustomerField(nameEt,surnameEt,usernameEt,passwordEt,adressEt)) {
                        User user = new User(name,surname,username,pasword);
                        Customer customer = new Customer(adress, user);
                        createCustomer(customer);

                    }
                    else {
                        Toast.makeText(RegisterActivity.this, "Popunite sva polja",
                                Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    if(validSellerFields(nameEt,surnameEt,usernameEt,passwordEt,
                            adressEt,emailEt,companyNameEt)) {
                        User user = new User(name,surname,username,pasword);
                        String email = emailEt.getText().toString().trim();
                        String companyName = companyNameEt.getText().toString().trim();
                        Seller seller = new Seller(email, adress, companyName, user);
                        createSeller(seller);
                    }
                    else {
                        Toast.makeText(RegisterActivity.this, "Popunite sva polja, email mora biti ispravan",
                                Toast.LENGTH_LONG).show();
                    }
                }
        }
    });


    }

        private void createCustomer(Customer customer){

            CustomerApiService customerApiService = RetrofitClientInstance.getRetrofitInstance(RegisterActivity.this).create(CustomerApiService.class);
            Call<ResponseBody> call = customerApiService.create(customer);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.code() == 201) {
                        Toast.makeText(RegisterActivity.this, "Registered", Toast.LENGTH_SHORT).show();
                        openLoginActivity();
                    }
                    else{
                        Toast.makeText(RegisterActivity.this, "Failed registration", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.i(TAG, t.toString());
                    Toast.makeText(RegisterActivity.this, "greska",Toast.LENGTH_SHORT).show();
                }
            });
        }

    private void createSeller(Seller seller){

        SellerApiService sellerApiService = RetrofitClientInstance.getRetrofitInstance(RegisterActivity.this).create(SellerApiService.class);
        Call<ResponseBody> call = sellerApiService.create(seller);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 201) {
                    Toast.makeText(RegisterActivity.this, "Registered", Toast.LENGTH_SHORT).show();
                    openLoginActivity();
                }
                else{
                    Toast.makeText(RegisterActivity.this, "Failed registration", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }

    public void openLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public boolean validCustomerField(EditText name,EditText surname,
                                   EditText username,EditText password,EditText adress){
        boolean valid = true;
        if(name.getText().toString().length() == 0 ) {
            name.setError("Name is required!");
            valid = false;
        }
        else {
            name.setError(null);
        }
        if(surname.getText().toString().length() == 0 ) {
            surname.setError("Surname is required!");
            valid = false;
        }
        else {
            surname.setError(null);
        }
        if(username.getText().toString().length() == 0 ) {
            username.setError("Username is required!");
            valid = false;
        }
        else {
            username.setError(null);
        }
        if(password.getText().toString().length() == 0 ) {
            password.setError("Password is required!");
            valid = false;
        }
        else {
            password.setError(null);
        }
        if(adress.getText().toString().trim().length() == 0 ) {
            adress.setError("Adress is required!");
            valid = false;
        }
        else {
            adress.setError(null);
        }

        return valid;
    }

    public boolean validSellerFields(EditText name,EditText surname,EditText username,
                                     EditText password,EditText adress,EditText email, EditText companyName){
        boolean valid = true;
        if(name.getText().toString().length() == 0 ) {
            name.setError("Name is required!");
            valid = false;
        }
        else {
            name.setError(null);
        }
        if(surname.getText().toString().length() == 0 ) {
            surname.setError("Surname is required!");
            valid = false;
        }
        else {
            surname.setError(null);
        }
        if(username.getText().toString().length() == 0 ) {
            username.setError("Username is required!");
            valid = false;
        }
        else {
            username.setError(null);
        }
        if(password.getText().toString().length() == 0 ) {
            password.setError("Password is required!");
            valid = false;
        }
        else {
            password.setError(null);
        }
        if(adress.getText().toString().length() == 0 ) {
            adress.setError("Adress is required!");
            valid = false;
        }
        else {
            adress.setError(null);
        }
        if(email.getText().toString().length() == 0 ) {
            email.setError("Email is required!");
            valid = false;
        }
        else {
            email.setError(null);
        }
        if(companyName.getText().toString().length() == 0 ) {
            companyName.setError("Company name is required!");
            valid = false;
        }
        else {
            companyName.setError(null);
        }
        String emailP = email.getText().toString().trim();
        if(isValidEmailAddress(emailP)){
            valid = true;
        }
        else {
            valid = false;
        }

        return valid;
    }
    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

}