package com.example.foodpanda.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;


import com.example.foodpanda.MainActivity;
import com.example.foodpanda.R;
import com.example.foodpanda.fragments.ArticlesFragment;

import com.example.foodpanda.fragments.ReviewsFragment;

import com.example.foodpanda.tools.FragmentTransition;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import java.text.DecimalFormat;

import model.Order;


public class DetailActivity extends AppCompatActivity {
    static final String TAG = DetailActivity.class.getSimpleName();
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Long id = getIntent().getLongExtra("id", 0);

        setTitle(getIntent().getStringExtra("name"));

        TextView adress = findViewById(R.id.adress_detail);
        TextView email = findViewById(R.id.email_detail);
        TextView avgRate = findViewById(R.id.avgRate_detail);



        avgRate.setText("Average rate: " + df2.format(getIntent().getDoubleExtra("avgRate", 0)));
        adress.setText("Adress: "+ getIntent().getStringExtra("adress"));
        email.setText("Email: " + getIntent().getStringExtra("email"));
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);

        Button endPrauchase = findViewById(R.id.endPrauchase);

        endPrauchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DetailActivity.this, CartActivity.class);
                startActivity(intent);

            }
        });

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                Fragment fragment = null;
                switch (tab.getPosition()) {
                    case 0:
                        fragment = new ReviewsFragment(id);
                        break;
                    case 1:
                        fragment = new ArticlesFragment();
                        break;

                }
                FragmentTransition.to(fragment, DetailActivity.this, false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        FragmentTransition.to(new ReviewsFragment(id), DetailActivity.this, false);

    }

        @Override
    public void onBackPressed() {
        Gson gson = new Gson();
        SharedPreferences preferences =  getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
        Order order  =  gson.fromJson(preferences.getString("Order",null), Order.class);
        if(order.getOrderItems().size() > 0) {


            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Warning!")
                    .setMessage("All unordered articles will be deleted from cart!")
                    .setCancelable(false)
                    .setPositiveButton("Back", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(DetailActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("Stay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
        else {
            super.onBackPressed();
        }
    }




}
