package com.example.foodpanda.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.MainActivity;
import com.example.foodpanda.R;
import com.example.foodpanda.fragments.CartFragment;
import com.example.foodpanda.fragments.ReviewsFragment;
import com.example.foodpanda.service.OrderService;
import com.example.foodpanda.tools.FragmentTransition;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import model.Order;
import model.OrderItem;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {
    private SensorManager mSensorManager;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;

    static final String TAG = CartActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        Gson gson = new Gson();
        SharedPreferences preferences =  getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
        Order order  =  gson.fromJson(preferences.getString("Order",null), Order.class);


        List<OrderItem> orderItems = new ArrayList<>();

        for(OrderItem o : order.getOrderItems()){
            orderItems.add(o);
        }

        double summaryPrice = 0;

        for(OrderItem o : orderItems){
                double price = o.getQuantity() * o.getArticle().getPrice();
                summaryPrice += price;
        }

        TextView price = findViewById(R.id.summary_price);

        price.setText("" + summaryPrice);

        Button orderBtn = findViewById(R.id.orderBtn);

        orderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                SharedPreferences preferences =  getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
                Order order  =  gson.fromJson(preferences.getString("Order",null), Order.class);
                if(order.getOrderItems().size() < 1){
                    Toast.makeText(CartActivity.this, "Cart is empty", Toast.LENGTH_SHORT).show();
                }
                else {
                    createOrder(order);
                }
            }
        });



        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Objects.requireNonNull(mSensorManager).registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        mAccel = 10f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;






        FragmentTransition.to(CartFragment.newInstance(), CartActivity.this, false);
    }

    private final SensorEventListener mSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta;
            if (mAccel > 12) {
                Gson gson = new Gson();
                SharedPreferences preferences =  getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
                Order order  =  gson.fromJson(preferences.getString("Order",null), Order.class);

                List<OrderItem> orderItems = new ArrayList<>();
                if(order.getOrderItems().size() > 0) {
                    orderItems = order.getOrderItems();
                    int last = orderItems.size() - 1;
                    orderItems.remove(last);


                    order.setOrderItems(orderItems);

                    String orderJson = gson.toJson(order);

                    preferences.edit().putString("Order", orderJson).apply();

                    finish();
                    startActivity(getIntent());
                    FragmentTransition.to(CartFragment.newInstance(), CartActivity.this, false);
                    Toast.makeText(getApplicationContext(), "Nice shake!Item removed", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(CartActivity.this, "Cart is empty", Toast.LENGTH_SHORT).show();
                }

            }
        }
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };


    @Override
    protected void onResume() {
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        super.onResume();
    }
    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }


    private void createOrder(Order order){

        OrderService orderService = RetrofitClientInstance.getRetrofitInstance(CartActivity.this).create(OrderService.class);
        Call<ResponseBody> call = orderService.create(order);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(CartActivity.this, "Order made", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(CartActivity.this, MainActivity.class);
                startActivity(intent);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });


    }
}