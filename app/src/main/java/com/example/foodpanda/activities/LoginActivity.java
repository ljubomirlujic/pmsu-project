package com.example.foodpanda.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.MainActivity;
import com.example.foodpanda.R;
import com.example.foodpanda.adapters.ArticleAdapter;
import com.example.foodpanda.fragments.ArticlesFragment;
import com.example.foodpanda.service.ArticleApiService;
import com.example.foodpanda.service.AuthenticationApiService;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import model.Article;
import model.LoginData;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    static final String TAG = ArticlesFragment.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        Button buttonLogin = (Button) findViewById(R.id.loginBtn);
        Button buttonRegister = (Button) findViewById(R.id.registerBtn);

        EditText username = findViewById(R.id.loginUsername);
        EditText password = findViewById(R.id.loginPassword);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean valid = validLoginFields(username, password);

                if(valid){
                    LoginData data = new LoginData();
                    data.setUsername(username.getText().toString().trim());
                    data.setPassword(password.getText().toString().trim());
                    loginApi(data);
                }


            }
        });

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRegisterAcitity();
            }
        });

    }

    public void openNewActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void openRegisterAcitity(){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void loginApi(LoginData loginData){
        AuthenticationApiService authenticationApiService = RetrofitClientInstance.getRetrofitInstance(LoginActivity.this).create(AuthenticationApiService.class);
        Call<ResponseBody> call = authenticationApiService.login(loginData);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if(response.code() == 200){
                        String token = response.body().string();
                        SharedPreferences preferences = getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
                        preferences.edit().putString("TOKEN", token).apply();
                        openNewActivity();
                        finish();
                    }
                    else if(response.code() == 403){
                        Toast.makeText(LoginActivity.this, "You are blocked", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(LoginActivity.this, "Username or password is incorrect", Toast.LENGTH_SHORT).show();
                    }

                }
                catch (NullPointerException e){
                    e.printStackTrace();
                }
                catch(IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });

    }

    public boolean validLoginFields(EditText username,EditText password){
        boolean valid = true;
        if(username.getText().toString().length() == 0 ) {
            username.setError("Username is required!");
            valid = false;
        }
        else {
            username.setError(null);
        }
        if(password.getText().toString().length() == 0 ) {
            password.setError("Password is required!");
            valid = false;
        }
        else {
            password.setError(null);
        }

        return valid;
    }



}