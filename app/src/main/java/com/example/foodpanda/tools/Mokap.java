package com.example.foodpanda.tools;

import com.example.foodpanda.R;

import java.util.ArrayList;
import java.util.List;

import model.Article;
import model.Comments;
import model.Restaurant;


public class Mokap {

    public static List<Restaurant> getRestaurants(){
        ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
        Restaurant u1 = new Restaurant("Papricica", "Jela sa rostillja", -1);
        Restaurant u2 = new Restaurant("Balans", "Slane i slatke palacinke", -1);
        Restaurant u3 = new Restaurant("Giros&Co", "Giros i piletina", -1);

        restaurants.add(u1);
        restaurants.add(u2);
        restaurants.add(u3);

        return restaurants;

    }

    public static List<Comments> getComments(){
        ArrayList<Comments> comments = new ArrayList<Comments>();
        Comments c1 = new Comments("Petar", "Peric", "Odlicna hrana, prijatan ambijent", 5);
        Comments c2 = new Comments("Milos", "Micic", "Odlicna hrana, ali se dugo ceka", 3);
        Comments c3 = new Comments("Darko", "Peric", "Losa hrana, visoka cijena, neljubazno osoblje", 1);

        comments.add(c1);
        comments.add(c2);
        comments.add(c3);

        return comments;

    }
//
//    public static List<Article> getArticles(){
//        ArrayList<Article> articles = new ArrayList<Article>();
//        Article a1 = new Article("Slatka palacinka","nutela plazma banana", 200,null);
//        Article a2 = new Article("Giros","pilece meso", 250,null);
//
//        articles.add(a1);
//        articles.add(a2);
//
//        return articles;
//    }

}
