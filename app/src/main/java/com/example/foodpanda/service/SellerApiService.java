package com.example.foodpanda.service;

import java.util.List;

import model.Seller;
import model.SellerDto;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface SellerApiService {

    @GET("/prodavci")
    Call<List<SellerDto>> getAll();

    @POST("/prodavci/registracija")
    Call<ResponseBody> create(@Body Seller seller);
}
