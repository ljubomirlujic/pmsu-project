package com.example.foodpanda.service;

import org.json.JSONObject;

import java.util.List;

import model.ChangePassword;
import model.User;
import model.UserDto;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserApiService {

    @POST("/korisnici/password")
    Call<ResponseBody> changePassword(@Body ChangePassword changePassword);

    @GET("/korisnici/{username}")
    Call<UserDto> getOne(@Path("username") String username);

    @GET("/korisnici")
    Call<List<UserDto>> getAll();

    @PUT("/korisnici/{id}")
    Call<UserDto> update(@Path("id") Long id, @Body UserDto userDto);

}
