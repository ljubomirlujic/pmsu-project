package com.example.foodpanda.service;

import model.LoginData;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthenticationApiService {

    @POST("/auth/login")
    Call<ResponseBody> login(@Body LoginData loginData);
}
