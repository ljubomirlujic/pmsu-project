package com.example.foodpanda.service;

import java.util.List;

import model.Article;
import model.ArticleList;
import model.Seller;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ArticleApiService {

    @GET("/articles")
    Call<List<Article>> getArticles();

    @GET("/articles/prodavac/{prodavac}")
    Call<List<Article>> getArticlesBySeller(@Path("prodavac") String prodavac);

    @GET("/articles/prodavac/{username}")
    Call<List<Article>> getArticlesByLoggedSeller(@Path("username") String username);

    @POST("/articles")
    Call<ResponseBody> create(@Body Article article);

    @DELETE("/articles/{id}")
    Call<ResponseBody> delete(@Path("id") Long id);

    @PUT("/articles/{id}")
    Call<ResponseBody> update(@Path("id") Long id, @Body Article article);
}
