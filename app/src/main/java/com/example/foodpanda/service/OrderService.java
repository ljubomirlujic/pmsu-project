package com.example.foodpanda.service;

import java.util.List;

import model.Order;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface OrderService {

    @POST("/porudzbine")
    Call<ResponseBody> create(@Body Order order);

    @GET("/porudzbine/kupac")
    Call<List<Order>> getAllByCustomer();


    @GET("/porudzbine/prodavac")
    Call<List<Order>> getAllByLoggedSeller();

    @GET("/porudzbine/prodavac/{id}")
    Call<List<Order>> getAllBySeller(@Path("id") Long id);

    @PUT("/porudzbine/{id}")
    Call<ResponseBody> update(@Path("id") Long id, @Body Order order);

    @PUT("/porudzbine/ocjena/{id}")
    Call<ResponseBody> rateUpdate(@Path("id") Long id, @Body Order order);
}
