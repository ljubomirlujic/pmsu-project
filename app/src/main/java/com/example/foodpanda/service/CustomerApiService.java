package com.example.foodpanda.service;

import model.Customer;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface CustomerApiService {

    @POST("/kupci/registracija")
    Call<ResponseBody> create(@Body Customer customer);
}
