package com.example.foodpanda.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.auth0.android.jwt.Claim;
import com.auth0.android.jwt.JWT;
import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.R;
import com.example.foodpanda.activities.LoginActivity;
import com.example.foodpanda.adapters.ArticleAdapter;
import com.example.foodpanda.service.ArticleApiService;
import com.example.foodpanda.service.AuthenticationApiService;
import com.example.foodpanda.service.UserApiService;
import com.example.foodpanda.tools.FragmentTransition;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import model.Article;
import model.ChangePassword;
import model.LoginData;
import model.User;
import model.UserDto;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {

    static final String TAG = ArticlesFragment.class.getSimpleName();
    private static UserDto user;
    EditText etName;
    EditText etSurname;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        EditText etOldPassword = view.findViewById(R.id.etOldPassword);
        EditText etNewPassword = view.findViewById(R.id.etNewPassword);

        Button change = view.findViewById(R.id.updateBtn_password);

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validPasswordFields(etOldPassword,etNewPassword)){
                    String oldPassword = etOldPassword.getText().toString().trim();
                    String newPassword = etNewPassword.getText().toString().trim();

                    ChangePassword changePassword = new ChangePassword(oldPassword,newPassword);

                    changePasswordApi(changePassword);


                }

            }
        });



        etName = view.findViewById(R.id.etName_info);
        etSurname = view.findViewById(R.id.etSurname_info);

        SharedPreferences preferences =  getActivity().getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
        String retrivedToken  = preferences.getString("TOKEN",null);
        JWT jwt = new JWT(retrivedToken);

        Claim username = jwt.getClaim("sub");

        getUser(username.asString());



        Button update = view.findViewById(R.id.updateBtn);
        Button edit = view.findViewById(R.id.editBtn);

        update.setVisibility(View.INVISIBLE);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validInfoFields(etName, etSurname)) {
                    user.setName(etName.getText().toString().trim());
                    user.setSurname(etSurname.getText().toString().trim());
                    updateUser(user.getId(), user);
                }
            }
        });

        etName.setEnabled(false);
        etSurname.setEnabled(false);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etName.isEnabled() && !etName.isEnabled()){
                    etName.setEnabled(true);
                    etSurname.setEnabled(true);
                    update.setVisibility(View.VISIBLE);
                }else {
                    etName.setEnabled(false);
                    etSurname.setEnabled(false);
                    update.setVisibility(View.INVISIBLE);
                }
            }
        });

        return view;
    }


    private void changePasswordApi(ChangePassword changePassword){
        UserApiService userApiService = RetrofitClientInstance.getRetrofitInstance(getActivity()).create(UserApiService.class);
        Call<ResponseBody> call = userApiService.changePassword(changePassword);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    Toast.makeText(getActivity(), "Password changed successfully", Toast.LENGTH_SHORT).show();
                    FragmentTransition.to(ProfileFragment.newInstance(),getActivity(),false);
                }
                else if(response.code() == 400){
                    Toast.makeText(getActivity(), "Passwords don't match", Toast.LENGTH_SHORT).show();
                }
                else{
                    System.out.println(response);
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });

    }

    private void getUser(String username){

        UserApiService userApiService = RetrofitClientInstance.getRetrofitInstance(getActivity()).create(UserApiService.class);
        Call<UserDto> call = userApiService.getOne(username);
        call.enqueue(new Callback<UserDto>() {
            @Override
            public void onResponse(Call<UserDto> call, Response<UserDto> response) {
                if(response.code() == 200){
                    user = response.body();
                    etName.setText(user.getName());
                    etSurname.setText(user.getSurname());
                }
                else if(response.code() == 404){
                    Toast.makeText(getActivity(), "User doesn't exist", Toast.LENGTH_SHORT).show();
                }
                else{
                    System.out.println(response);
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<UserDto> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }

    private void updateUser(Long id, UserDto userDto){

        UserApiService userApiService = RetrofitClientInstance.getRetrofitInstance(getActivity()).create(UserApiService.class);
        Call<UserDto> call = userApiService.update(id, userDto);
        call.enqueue(new Callback<UserDto>() {
            @Override
            public void onResponse(Call<UserDto> call, Response<UserDto> response) {
                if(response.code() == 200){
                    Toast.makeText(getActivity(), "Sucessfully updated", Toast.LENGTH_SHORT).show();
                    FragmentTransition.to(ProfileFragment.newInstance(),getActivity(),false);
                }
                else if(response.code() == 404){
                    Toast.makeText(getActivity(), "User doesn't exist", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<UserDto> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }


    public boolean validPasswordFields(EditText oldPassword,EditText newPassword){
        boolean valid = true;
        if(oldPassword.getText().toString().length() == 0 ) {
            oldPassword.setError("Old password is required!");
            valid = false;
        }
        else {
            oldPassword.setError(null);
        }
        if(newPassword.getText().toString().length() == 0 ) {
            newPassword.setError("New password is required!");
            valid = false;
        }
        else {
            newPassword.setError(null);
        }

        return valid;
    }

    public boolean validInfoFields(EditText etName,EditText etSurname){
        boolean valid = true;
        if(etName.getText().toString().length() == 0 ) {
            etName.setError("Name is required!");
            valid = false;
        }
        else {
            etName.setError(null);
        }
        if(etSurname.getText().toString().length() == 0 ) {
            etSurname.setError("Surname is required!");
            valid = false;
        }
        else {
            etSurname.setError(null);
        }

        return valid;
    }
}