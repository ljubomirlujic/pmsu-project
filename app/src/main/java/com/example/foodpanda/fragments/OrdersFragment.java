package com.example.foodpanda.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.R;
import com.example.foodpanda.adapters.ReviewsOrderAdapter;
import com.example.foodpanda.service.OrderService;
import com.example.foodpanda.tools.FragmentTransition;

import java.util.List;

import model.Order;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.view.View.*;

public class OrdersFragment extends Fragment{

    private RecyclerView recyclerView;
    static final String TAG = OrdersFragment.class.getSimpleName();

    public static OrdersFragment newInstance() {
        return new OrdersFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle data) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.recycler_view, vg, false);

        recyclerView = view.findViewById(R.id.recycle_v);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getOrders();
    }


    private void getOrders(){


        OrderService orderService = RetrofitClientInstance.getRetrofitInstanceOrders(getActivity()).create(OrderService.class);

        Call<List<Order>> call = orderService.getAllByCustomer();
        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {

                recyclerView.setAdapter(new ReviewsOrderAdapter(response.body(), getContext(), getActivity()));

            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }


}
