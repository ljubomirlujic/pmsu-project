package com.example.foodpanda.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.foodpanda.R;
import com.example.foodpanda.adapters.ArticleAdapter;
import com.example.foodpanda.adapters.CartItemAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import model.Order;
import model.OrderItem;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CartFragment extends Fragment {
    private RecyclerView recyclerView;

    public static CartFragment newInstance() {
        CartFragment fragment = new CartFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.recycler_view, vg, false);
        recyclerView = view.findViewById(R.id.recycle_v);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        Gson gson = new Gson();
        SharedPreferences preferences =  getActivity().getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
        Order order  =  gson.fromJson(preferences.getString("Order",null), Order.class);


        List<OrderItem> orderItems = new ArrayList<>();

        for(OrderItem o : order.getOrderItems()){
            orderItems.add(o);
        }

        recyclerView.setAdapter(new CartItemAdapter(orderItems, getContext(),getActivity()));
        return view;
    }
}