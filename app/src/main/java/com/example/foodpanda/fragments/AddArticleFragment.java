package com.example.foodpanda.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.content.CursorLoader;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.MainActivity;
import com.example.foodpanda.R;
import com.example.foodpanda.activities.LoginActivity;
import com.example.foodpanda.activities.RegisterActivity;
import com.example.foodpanda.activities.SplashScreenActivity;
import com.example.foodpanda.adapters.ArticleAdapter;
import com.example.foodpanda.service.ArticleApiService;
import com.example.foodpanda.tools.FragmentTransition;

import java.io.File;
import java.util.List;

import model.Article;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddArticleFragment  extends Fragment {

    static final String TAG = RegisterActivity.class.getSimpleName();
    String imagePath;



    public static AddArticleFragment newInstance() {
        return new AddArticleFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle data) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.add_article_layout, vg, false);
        EditText nameEt = view.findViewById(R.id.nameArticle_add);
        EditText descEt = view.findViewById(R.id.descArticle_add);
        EditText priceEt = view.findViewById(R.id.priceArticle_add);



        Button add = (Button) view.findViewById(R.id.buttonAdd_article);
        Button upload = (Button)view.findViewById(R.id.btnUpload_article);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validArticleField(nameEt, descEt, priceEt)) {
                    String name = nameEt.getText().toString().trim();
                    String desc = descEt.getText().toString().trim();
                    double price = Double.valueOf(priceEt.getText().toString().trim());
                    Article article = new Article("",name, desc, price, null);
                    createArticle(article);
                }
                else{
                    Toast.makeText(getActivity(), "Fill in all the fields correctly",Toast.LENGTH_SHORT).show();
                }
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).selectImage();
            }
        });

        return view;
    }






    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        // ovo korostimo ako je nasa arhitekrura takva da imamo jednu aktivnost
        // i vise fragmentaa gde svaki od njih ima svoj menu unutar toolbar-a
        menu.clear();
        inflater.inflate(R.menu.activity_itemdetail, menu);
    }



    private void createArticle(Article article){

        String image = ((MainActivity)getActivity()).convertToString();
        article.setImage(image);

        ArticleApiService articleApiService = RetrofitClientInstance.getRetrofitInstance(getActivity()).create(ArticleApiService.class);
        Call<ResponseBody> call = articleApiService.create(article);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 201) {
                    Toast.makeText(getActivity(), "Article created", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("position", 0);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getActivity(), "Be sure all fields are filled", Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }

    public boolean validArticleField(EditText name, EditText desc, EditText price){

        boolean valid = true;

        if(name.getText().toString().length() == 0 ) {
            name.setError("Name is required!");
            valid = false;
        }
        else {
            name.setError(null);
        }
        if(desc.getText().toString().length() == 0 ) {
            desc.setError("Description is required!");
            valid = false;
        }
        else {
            desc.setError(null);
        }

        if(price.getText().toString().length() == 0 ) {
            price.setError("Price is requered!");
            valid = false;
        }
        else {
            price.setError(null);
        }
        try{
            Double.valueOf(String.valueOf(price.getText().toString().trim()));
        }catch (Exception e){
            price.setError("Price isn't correct!");
            valid = false;
        }

        return valid;
    }

}