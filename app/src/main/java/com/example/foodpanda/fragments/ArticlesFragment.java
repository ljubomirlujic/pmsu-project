package com.example.foodpanda.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.auth0.android.jwt.Claim;
import com.auth0.android.jwt.JWT;
import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.R;
import com.example.foodpanda.adapters.ArticleAdapter;
import com.example.foodpanda.service.ArticleApiService;

import java.util.List;

import model.Article;
import model.ArticleList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ArticlesFragment  extends Fragment {
    private RecyclerView recyclerView;

    static final String TAG = ArticlesFragment.class.getSimpleName();

    public static ArticlesFragment newInstance() {
        return new ArticlesFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle data) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.recycler_view, vg, false);
        recyclerView = view.findViewById(R.id.recycle_v);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        // ovo korostimo ako je nasa arhitekrura takva da imamo jednu aktivnost
        // i vise fragmentaa gde svaki od njih ima svoj menu unutar toolbar-a
        menu.clear();
        inflater.inflate(R.menu.activity_itemdetail, menu);
    }

    @Override
    public void onResume(){
        super.onResume();
        getActivity().getTitle();
        if(getActivity().getTitle().equals("foodpanda")){
            SharedPreferences preferences =  getActivity().getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
            String retrivedToken  = preferences.getString("TOKEN",null);

            JWT jwt = new JWT(retrivedToken);
            Claim username = jwt.getClaim("sub");

            getArticles(username.asString());
        }else {
            getArticles(getActivity().getTitle().toString());
        }
    }

    private void getArticles(String seller){

        ArticleApiService articleApiService = RetrofitClientInstance.getRetrofitInstance(getActivity()).create(ArticleApiService.class);
        Call<List<Article>> call = articleApiService.getArticlesBySeller(seller);
        call.enqueue(new Callback<List<Article>>() {
            @Override
            public void onResponse(Call<List<Article>> call, Response<List<Article>> response) {

                recyclerView.setAdapter(new ArticleAdapter(response.body(), getContext(),getActivity()));
            }

            @Override
            public void onFailure(Call<List<Article>> call, Throwable t) {

            }
        });
    }





}