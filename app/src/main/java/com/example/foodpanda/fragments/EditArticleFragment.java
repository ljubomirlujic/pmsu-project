package com.example.foodpanda.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.MainActivity;
import com.example.foodpanda.R;
import com.example.foodpanda.adapters.ArticleAdapter;
import com.example.foodpanda.service.ArticleApiService;
import com.example.foodpanda.tools.FragmentTransition;

import java.util.List;

import model.Article;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class EditArticleFragment extends Fragment {
    private RecyclerView recyclerView;
    Article article;

    static final String TAG = ArticlesFragment.class.getSimpleName();

    public static EditArticleFragment newInstance() {
        return new EditArticleFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle bundle = this.getArguments();
        if (bundle != null) {
             article = bundle.getParcelable("article"); // Key
        }

        View view = inflater.inflate(R.layout.fragment_edit_article, container, false);
        EditText name = view.findViewById(R.id.nameArticle_update);
        EditText description = view.findViewById(R.id.descArticle_update);
        EditText price = view.findViewById(R.id.priceArticle_update);
        Button updateBtn = view.findViewById(R.id.buttonEdit_article);
        Button uploadBtn = view.findViewById(R.id.buttonUploadUpdate_article);

        name.setText(article.getName());
        description.setText(article.getDescription());
        price.setText(String.valueOf(article.getPrice()));



        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Long id = article.getId();

                article.setName(name.getText().toString());
                article.setDescription(description.getText().toString());
                article.setPrice(Double.valueOf(price.getText().toString()));

                updateArticle(id, article);
            }
        });

        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).updateImage();
            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void updateArticle(Long id, Article article){

        String image = ((MainActivity)getActivity()).convertToString();
        if(image != null) {
            article.setImage(image);
        }

        ArticleApiService articleApiService = RetrofitClientInstance.getRetrofitInstance(getActivity()).create(ArticleApiService.class);
        Call<ResponseBody> call = articleApiService.update(id, article);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                FragmentTransition.to(ArticlesFragment.newInstance(), getActivity(), false);
                Toast.makeText(getActivity(), "Article updated", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }
}