package com.example.foodpanda.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.R;
import com.example.foodpanda.adapters.SellerAdapter;
import com.example.foodpanda.adapters.UserAdapter;
import com.example.foodpanda.service.SellerApiService;
import com.example.foodpanda.service.UserApiService;

import java.util.List;

import model.Seller;
import model.UserDto;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UsersListFragment extends Fragment {

    private RecyclerView recyclerView;
    static final String TAG = UsersListFragment.class.getSimpleName();

    public static UsersListFragment newInstance() {
        return new UsersListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle data) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.recycler_view, vg, false);
        recyclerView = view.findViewById(R.id.recycle_v);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));



        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        // ovo korostimo ako je nasa arhitekrura takva da imamo jednu aktivnost
        // i vise fragmentaa gde svaki od njih ima svoj menu unutar toolbar-a
        menu.clear();
        inflater.inflate(R.menu.activity_itemdetail, menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        getUsers();
    }


    private void getUsers(){


        UserApiService sellerApiService = RetrofitClientInstance.getRetrofitInstance(getActivity()).create(UserApiService.class);
        Call<List<UserDto>> call = sellerApiService.getAll();
        call.enqueue(new Callback<List<UserDto>>() {
            @Override
            public void onResponse(Call<List<UserDto>> call, Response<List<UserDto>> response) {

                recyclerView.setAdapter(new UserAdapter(response.body(), getContext(), getActivity()));

            }

            @Override
            public void onFailure(Call<List<UserDto>> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }



}