package com.example.foodpanda.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.R;
import com.example.foodpanda.adapters.CommentAdapter;
import com.example.foodpanda.adapters.ReviewsOrderAdapter;
import com.example.foodpanda.service.OrderService;
import com.example.foodpanda.tools.Mokap;

import java.util.ArrayList;
import java.util.List;

import model.Order;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReviewsFragment extends Fragment {
    Long id;
    private RecyclerView recyclerView;
    static final String TAG = OrdersFragment.class.getSimpleName();


    public ReviewsFragment(Long id) {
        this.id = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle data) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.recycler_view, vg, false);

        recyclerView = view.findViewById(R.id.recycle_v);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getComments(id);
    }


    private void getComments(Long id){


        OrderService orderService = RetrofitClientInstance.getRetrofitInstanceOrders(getActivity()).create(OrderService.class);

        Call<List<Order>> call = orderService.getAllBySeller(id);
        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {

                recyclerView.setAdapter(new CommentAdapter(response.body(), getContext(), getActivity()));

            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }



}