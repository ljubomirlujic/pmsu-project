package com.example.foodpanda.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.foodpanda.MainActivity;
import com.example.foodpanda.R;
import com.example.foodpanda.tools.FragmentTransition;

import static android.view.View.*;

public class DiscountFragment extends Fragment implements OnClickListener {

    public static DiscountFragment newInstance() {
        return new DiscountFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle data) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.discount_layout, vg, false);

        Button button = (Button) view.findViewById(R.id.discont_addBtn);
        button.setOnClickListener(this);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        // ovo korostimo ako je nasa arhitekrura takva da imamo jednu aktivnost
        // i vise fragmentaa gde svaki od njih ima svoj menu unutar toolbar-a
        menu.clear();
        inflater.inflate(R.menu.activity_itemdetail, menu);
    }

    @Override
    public void onClick(View v) {
        FragmentTransition.to(AddDiscountFragment.newInstance(), getActivity(), true);
    }
}
