package com.example.foodpanda.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.R;
import com.example.foodpanda.activities.DetailActivity;
import com.example.foodpanda.adapters.ArticleAdapter;
import com.example.foodpanda.adapters.SellerAdapter;
import com.example.foodpanda.service.ArticleApiService;
import com.example.foodpanda.service.SellerApiService;
import com.example.foodpanda.tools.Mokap;
import com.google.gson.Gson;

import java.util.List;

import model.Article;
import model.Order;
import model.Restaurant;
import model.Seller;
import model.SellerDto;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MyFragment extends Fragment {
    private RecyclerView recyclerView;

    static final String TAG = MyFragment.class.getSimpleName();

	public static MyFragment newInstance() {
        return new MyFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle data) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.recycler_view, vg, false);
        recyclerView = view.findViewById(R.id.recycle_v);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));



        Order order = new Order();
        order.setAnonymousComment(false);
        order.setArchivedComment(false);
        order.setDelivered(false);
        Gson gson = new Gson();
        String orderJson = gson.toJson(order);

        SharedPreferences preferences = getActivity().getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
        preferences.edit().putString("Order", orderJson).apply();

		return view;

	}

//    @Override
//    public void onListItemClick(ListView l, View v, int position, long id) {
//        super.onListItemClick(l, v, position, id);
//
//        Restaurant restaurant = Mokap.getRestaurants().get(position);
//
//        /*
//        * Ako nasoj aktivnosti zelimo da posaljemo nekakve podatke
//        * za to ne treba da koristimo konstruktor. Treba da iskoristimo
//        * identicnu strategiju koju smo koristili kda smo radili sa
//        * fragmentima! Koristicemo Bundle za slanje podataka. Tacnije
//        * intent ce formirati Bundle za nas, ali mi treba da pozovemo
//        * odgovarajucu putExtra metodu.
//        * */
//        Intent intent = new Intent(getActivity(), DetailActivity.class);
//        intent.putExtra("name", restaurant.getName());
//        startActivity(intent);
//    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.clear();
        inflater.inflate(R.menu.activity_itemdetail, menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        getSellers();
    }

    private void getSellers(){


        SellerApiService sellerApiService = RetrofitClientInstance.getRetrofitInstance(getActivity()).create(SellerApiService.class);
        Call<List<SellerDto>> call = sellerApiService.getAll();
        call.enqueue(new Callback<List<SellerDto>>() {
            @Override
            public void onResponse(Call<List<SellerDto>> call, Response<List<SellerDto>> response) {
                recyclerView.setAdapter(new SellerAdapter(getActivity(),response.body(), getContext()));

            }

            @Override
            public void onFailure(Call<List<SellerDto>> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }

    //    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        return super.onOptionsItemSelected(item);
//    }
}