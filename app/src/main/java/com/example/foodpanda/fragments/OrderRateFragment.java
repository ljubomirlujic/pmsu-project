package com.example.foodpanda.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.R;
import com.example.foodpanda.service.OrderService;
import com.example.foodpanda.tools.FragmentTransition;

import java.nio.file.Path;

import model.Order;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderRateFragment  extends Fragment {
    Order order;
    int num;
    int myRating = 0;
    static final String TAG = OrderRateFragment.class.getSimpleName();

    public OrderRateFragment(Order order, int num) {
        this.order = order;
        this.num = num;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle data) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.rate_order_layout, vg, false);

        TextView orderNum = view.findViewById(R.id.order_num_rate);
        EditText comment = view.findViewById(R.id.etComment);
        RatingBar rate = view.findViewById(R.id.rating);
        Button rateBtn = view.findViewById(R.id.rateBtn_rate);
        CheckBox anonymous = view.findViewById(R.id.checkbox_anonymous);

        orderNum.setText("Order: " + num);


        rate.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float r, boolean fromUser) {
                int rating = (int) r;
                String message = null;

                myRating = (int) rate.getRating();

                switch (rating){
                    case 1:
                        message = "Sorry to hear that :(";
                        break;
                    case 2:
                        message = "Okey";
                        break;
                    case 3:
                        message = "Good enough";
                        break;
                    case 4:
                        message = "Very good";
                        break;
                    case 5:
                        message = "Awasome!:)";
                        break;
                }
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

            }
        });

        rateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validFields(comment, rate)){
                    order.setComment(comment.getText().toString().trim());
                    order.setRate(myRating);
                    if(anonymous.isChecked()){
                        order.setAnonymousComment(true);
                    }
                    updateOrder(order.getId(), order);

                }
            }
        });



        return view;
    }


    private void updateOrder(Long id, Order order){


        OrderService orderService = RetrofitClientInstance.getRetrofitInstance(getActivity()).create(OrderService.class);

        Call<ResponseBody> call = orderService.rateUpdate(id, order);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.code() == 200){
                    FragmentTransition.to(OrdersFragment.newInstance(), getActivity(), true);
                }else if(response.code() == 404){
                    Toast.makeText(getActivity(), "Order doesn't exist", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }


    public boolean validFields(EditText comment,RatingBar rate){
        boolean valid = true;
        if(comment.getText().toString().length() == 0 ) {
            comment.setError("Comment is required!");
            valid = false;
        }
        else {
            comment.setError(null);
        }
        if(rate.getRating() == 0) {
            Toast.makeText(getActivity(), "Rate is required!", Toast.LENGTH_SHORT).show();
            valid = false;
        }

        return valid;
    }

}