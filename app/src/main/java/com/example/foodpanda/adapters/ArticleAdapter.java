package com.example.foodpanda.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.ApiClient.TokenUtils;
import com.example.foodpanda.MainActivity;
import com.example.foodpanda.R;
import com.example.foodpanda.activities.DetailActivity;
import com.example.foodpanda.fragments.AddArticleFragment;
import com.example.foodpanda.fragments.ArticlesFragment;
import com.example.foodpanda.fragments.EditArticleFragment;
import com.example.foodpanda.service.ArticleApiService;
import com.example.foodpanda.tools.FragmentTransition;
import com.example.foodpanda.tools.Mokap;
import com.google.gson.Gson;

import java.util.List;

import model.Article;
import model.Order;
import model.OrderItem;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ArticleAdapter  extends RecyclerView.Adapter<ArticleAdapter.RecyclerViewHolder> {
    private List<Article> aData;
    private Context context;
    private FragmentActivity fragmentActivity;
    static final String TAG = ArticlesFragment.class.getSimpleName();
    private FragmentActivity activity;

    public ArticleAdapter(List<Article> data,Context context,FragmentActivity activity) {
        this.aData = data;
        this.context = context;
        this.activity = activity;
        this.fragmentActivity = activity;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.articles_list, null);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        Article article = aData.get(position);


        holder.name.setText(article.getName());
        holder.description.setText(article.getDescription());
        holder.price.setText("" + article.getPrice());
        byte[] bytes = Base64.decode(article.getImage(),Base64.DEFAULT);

        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
        holder.image.setImageBitmap(bitmap);


        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteArticle(Long.valueOf(article.getId()));
            }
        });

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Article editArticle = new Article();
                editArticle.setId(article.getId());
                editArticle.setName(holder.name.getText().toString());
                editArticle.setDescription(holder.description.getText().toString());
                editArticle.setPrice(Double.valueOf(holder.price.getText().toString()));
                editArticle.setSeller(article.getSeller());
                editArticle.setImage(article.getImage());
                Fragment fragment = EditArticleFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putParcelable("article",  article);
                fragment.setArguments(bundle);

                FragmentTransition.to(fragment, activity, true);
            }
        });

        holder.cartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                SharedPreferences preferences =  activity.getSharedPreferences("MY_APP", Context.MODE_PRIVATE);
                Order order  =  gson.fromJson(preferences.getString("Order",null), Order.class);


                Article articleOrder = new Article();

                articleOrder.setId(article.getId());
                articleOrder.setName(holder.name.getText().toString());
                articleOrder.setDescription(holder.description.getText().toString());
                articleOrder.setPrice(Double.valueOf(holder.price.getText().toString()));

                int quantity = Integer.valueOf(holder.quantity.getText().toString().trim());

                OrderItem orderItem = new OrderItem(null, quantity, articleOrder);

                order.getOrderItems().add(orderItem);

                String orderJson = gson.toJson(order);

                preferences.edit().putString("Order", orderJson).apply();

                holder.quantity.setText("");

                Toast.makeText(activity, "Article added to cart", Toast.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    public int getItemCount() {
        if(aData != null) {
            return aData.size();
        }
        return 0;
    }



    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView description;
        ImageView image;
        TextView price;
        Button deleteButton;
        Button editButton;
        Button cartButton;
        EditText quantity;
        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            name = (TextView)itemView.findViewById(R.id.article_name);
            description = (TextView)itemView.findViewById(R.id.article_desc);
            image = (ImageView)itemView.findViewById(R.id.imageView_article);
            price = (TextView)itemView.findViewById(R.id.article_price);
            quantity = itemView.findViewById(R.id.etQunantity);
            deleteButton = itemView.findViewById(R.id.deleteArticle_button);
            editButton = itemView.findViewById(R.id.updateArticle_button);
            cartButton = itemView.findViewById(R.id.cartAdd_button);
            String role = TokenUtils.getRole(activity);
            if(role.equals("KUPAC")){
                deleteButton.setVisibility(View.INVISIBLE);
                editButton.setVisibility(View.INVISIBLE);
                quantity.setHint("quantity");
            }
            if(role.equals("PRODAVAC")){
                cartButton.setVisibility(View.INVISIBLE);
                quantity.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void deleteArticle(Long id){

        ArticleApiService articleApiService = RetrofitClientInstance.getRetrofitInstance(activity).create(ArticleApiService.class);
        Call<ResponseBody> call = articleApiService.delete(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                FragmentTransition.to(ArticlesFragment.newInstance(), activity, false);
                Toast.makeText(activity, "Article deleted", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });


    }



}