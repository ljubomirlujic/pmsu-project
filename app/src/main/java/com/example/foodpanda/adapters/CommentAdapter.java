package com.example.foodpanda.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.ApiClient.TokenUtils;
import com.example.foodpanda.MainActivity;
import com.example.foodpanda.R;
import com.example.foodpanda.activities.DetailActivity;
import com.example.foodpanda.fragments.CommentListFragment;
import com.example.foodpanda.fragments.OrdersFragment;
import com.example.foodpanda.fragments.ReviewsFragment;
import com.example.foodpanda.service.OrderService;
import com.example.foodpanda.tools.FragmentTransition;
import com.example.foodpanda.tools.Mokap;

import java.util.List;

import model.Comments;
import model.Order;
import model.Restaurant;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentAdapter  extends RecyclerView.Adapter<CommentAdapter.RecyclerViewHolder> {
    static final String TAG = ReviewsFragment.class.getSimpleName();

    private List<Order> oData;
    private Context context;
    private FragmentActivity activity;

    public CommentAdapter(List<Order> oData, Context context, FragmentActivity activity) {
        this.oData = oData;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_list, null);

        return new CommentAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        Order order = oData.get(position);
        String role = TokenUtils.getRole(activity);

        if(role.equals("KUPAC") && order.isArchivedComment()){
            holder.comment.setVisibility(View.GONE);
        }

        if(!order.isAnonymousComment()) {
            String name = order.getCustomer().getUser().getName() + " " + order.getCustomer().getUser().getSurname();
            holder.name.setText(name);
        }
        else {
            holder.name.setText("Anonymous");
        }
        holder.text.setText(order.getComment());
        holder.rate.setRating(order.getRate());



        if(order.isArchivedComment() && role.equals("PRODAVAC")){
            holder.archive.setVisibility(View.INVISIBLE);
            holder.archived.setVisibility(View.VISIBLE);
        }
        else if(!order.isArchivedComment() && role.equals("PRODAVAC")){
            holder.archived.setVisibility(View.INVISIBLE);
            holder.archive.setVisibility(View.VISIBLE);
        }
        else {
            holder.archived.setVisibility(View.INVISIBLE);
            holder.archive.setVisibility(View.INVISIBLE);
        }


        holder.archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order.setArchivedComment(true);
                updateOrder(order.getId(), order);

            }
        });
    }

    @Override
    public int getItemCount() {
        if(oData != null){
            return oData.size();
        }
        return 0;
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        RatingBar rate;
        TextView text;
        TextView archived;
        Button archive;
        CardView comment;
        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name_comments);
            rate = itemView.findViewById(R.id.rate_comments);
            text = itemView.findViewById(R.id.description_comments);
            archived = itemView.findViewById(R.id.tv_archived);
            archive = itemView.findViewById(R.id.archiveBtn);
            comment = itemView.findViewById(R.id.cardView_comment);

        }
    }

    private void updateOrder(Long id, Order order){


        OrderService orderService = RetrofitClientInstance.getRetrofitInstance(activity).create(OrderService.class);

        Call<ResponseBody> call = orderService.update(id, order);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.code() == 200){
                    FragmentTransition.to(CommentListFragment.newInstance(), activity, true);
                    Toast.makeText(activity, "Archived", Toast.LENGTH_SHORT).show();
                }else if(response.code() == 404){
                    Toast.makeText(activity, "Order doesn't exist", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }


}
