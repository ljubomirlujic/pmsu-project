package com.example.foodpanda.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodpanda.ApiClient.TokenUtils;
import com.example.foodpanda.R;
import com.example.foodpanda.fragments.ArticlesFragment;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import model.Article;
import model.Order;
import model.OrderItem;

public class CartItemAdapter extends RecyclerView.Adapter<CartItemAdapter.RecyclerViewHolder>{

    private List<OrderItem> aData;
    private Context context;
    static final String TAG = CartItemAdapter.class.getSimpleName();
    private FragmentActivity activity;

    public CartItemAdapter(List<OrderItem> data, Context context, FragmentActivity activity) {
        super();
        this.aData = data;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item_layout, null);


        return new CartItemAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        OrderItem orderItem = aData.get(position);

        holder.name.setText("Name: " + orderItem.getArticle().getName());
        holder.description.setText("Description: " + orderItem.getArticle().getDescription());
        holder.price.setText("Item price: " + orderItem.getArticle().getPrice());
        holder.quantity.setText("Quantity: " + orderItem.getQuantity());
    }

    @Override
    public int getItemCount() {
        if(aData != null) {
            return aData.size();
        }
        return 0;
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView description;
        TextView price;
        TextView quantity;
        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.article_name_cart);
            description = (TextView)itemView.findViewById(R.id.article_desc_cart);
            price = (TextView)itemView.findViewById(R.id.article_price_cart);
            quantity = itemView.findViewById(R.id.article_quantity_cart);

        }
    }
}
