package com.example.foodpanda.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodpanda.R;
import com.example.foodpanda.activities.DetailActivity;

import java.text.DecimalFormat;
import java.util.List;

import model.Seller;
import model.SellerDto;


/*
* Adapteri unutar Android-a sluze da prikazu unapred nedefinisanu kolicinu podataka
* pristigle sa interneta ili ucitane iz baze ili filesystem-a uredjaja.
* Da bi napravili adapter treba da napraivmo klasu, koja nasledjuje neki od postojecih adaptera.
* Za potrebe ovih vezbi koristicemo BaseAdapter koji je sposoban da kao izvor podataka iskoristi listu ili niz.
* Nasledjivanjem bilo kog adaptera, dobicemo
* nekolkko metoda koje moramo da referinisemo da bi adapter ispravno radio.
* */
public class SellerAdapter extends RecyclerView.Adapter<SellerAdapter.RecyclerViewHolder> {
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    private List<SellerDto> sData;
    private Context context;
    private Activity activity;

    public SellerAdapter(Activity activity,List<SellerDto> data, Context context) {
        this.sData = data;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.seller_list, null);

        return new SellerAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
            SellerDto seller = sData.get(position);

            holder.name.setText(seller.getCompanyName());
            holder.adress.setText(seller.getAdress());
            holder.avgRate.setText("Average rate: " + df2.format(seller.getAvgRate()));

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, DetailActivity.class);
                    intent.putExtra("id", seller.getUser().getId());
                    intent.putExtra("name", seller.getCompanyName());
                    intent.putExtra("adress", seller.getAdress());
                    intent.putExtra("email", seller.getEmail());
                    intent.putExtra("avgRate", seller.getAvgRate());
                    context.startActivity(intent);
                }
            });
    }

    @Override
    public int getItemCount() {
        if(sData != null){
            return sData.size();
        }
        return 0;
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView adress;
        TextView avgRate;
        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

        name = (TextView)itemView.findViewById(R.id.name_seller);
        adress = (TextView)itemView.findViewById(R.id.adress_seller);
        avgRate = itemView.findViewById(R.id.rate_seller);

        }
    }



}
