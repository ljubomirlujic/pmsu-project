package com.example.foodpanda.adapters;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.R;
import com.example.foodpanda.fragments.OrderRateFragment;
import com.example.foodpanda.fragments.OrdersFragment;
import com.example.foodpanda.service.OrderService;
import com.example.foodpanda.tools.FragmentTransition;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.Article;
import model.Order;
import model.OrderItem;
import model.UserDto;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReviewsOrderAdapter extends RecyclerView.Adapter<ReviewsOrderAdapter.RecyclerViewHolder> {

    static final String TAG = OrdersFragment.class.getSimpleName();

    private List<Order> oData;
    private Context context;
    private FragmentActivity activity;


    public ReviewsOrderAdapter(List<Order> oData, Context context, FragmentActivity activity) {
        this.oData = oData;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_orders_list, null);

        return new ReviewsOrderAdapter.RecyclerViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        Order order = oData.get(position);



        List<String> arti = new ArrayList<>();

        for(OrderItem o : order.getOrderItems()){
           arti.add(o.getQuantity() + " x " + o.getArticle().getName());
        }
        String articles = String.join(", ", arti);




        String time = order.getTime().replace("T", " ");

        holder.time.setText("Date and time: " + time);
        holder.articles.setText("Articles: " + articles);

        holder.num.setText("Order: " + (position + 1));

        holder.rated.setVisibility(View.INVISIBLE);


        if(!order.isDelivered()) {
            holder.rate.setVisibility(View.INVISIBLE);
            holder.delivered.setVisibility(View.VISIBLE);
        }
        else if(order.isDelivered() && order.getRate() == 0){
            holder.rate.setVisibility(View.VISIBLE);
            holder.delivered.setVisibility(View.INVISIBLE);
        }
        else{
            holder.rate.setVisibility(View.INVISIBLE);
            holder.delivered.setVisibility(View.INVISIBLE);
            holder.rated.setText("RATED: " + order.getRate());
            holder.rated.setVisibility(View.VISIBLE);
        }

        holder.delivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order.setDelivered(true);
                updateOrder(order.getId(), order);

            }
        });

        holder.rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(new OrderRateFragment(order, (position+1)), activity, true);
            }
        });
    }



    @Override
    public int getItemCount() {
        if(oData != null){
            return oData.size();
        }
        return 0;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView num;
        TextView time;
        TextView articles;
        TextView rated;
        Button delivered;
        Button rate;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            num = itemView.findViewById(R.id.number_rev);
            time = itemView.findViewById(R.id.time_rev);
            rated = itemView.findViewById(R.id.rated_rev);
            articles = itemView.findViewById(R.id.description_rev);
            delivered = itemView.findViewById(R.id.deliveredBtn);
            rate = itemView.findViewById(R.id.rateBtn);




        }
    }

    private void updateOrder(Long id, Order order){


        OrderService orderService = RetrofitClientInstance.getRetrofitInstance(activity).create(OrderService.class);

        Call<ResponseBody> call = orderService.update(id, order);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.code() == 200){
                    FragmentTransition.to(OrdersFragment.newInstance(), activity, true);
                }else if(response.code() == 404){
                    Toast.makeText(activity, "Order doesn't exist", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }
}
