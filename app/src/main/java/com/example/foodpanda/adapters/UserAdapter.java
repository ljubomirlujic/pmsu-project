package com.example.foodpanda.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.example.foodpanda.ApiClient.RetrofitClientInstance;
import com.example.foodpanda.R;
import com.example.foodpanda.fragments.ProfileFragment;
import com.example.foodpanda.fragments.UsersListFragment;
import com.example.foodpanda.service.UserApiService;
import com.example.foodpanda.tools.FragmentTransition;

import java.util.List;


import model.UserDto;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.RecyclerViewHolder> {
    static final String TAG = UsersListFragment.class.getSimpleName();
    private List<UserDto> uData;
    private Context context;
    private FragmentActivity activity;

    public UserAdapter(List<UserDto> uData, Context context, FragmentActivity activity) {
        this.uData = uData;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.users_list_layout, null);

        return new UserAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        UserDto user = uData.get(position);

        holder.username.setText(user.getUsername());
        holder.name.setText(user.getName());
        holder.surname.setText(user.getUsername());
        if(!user.isBlocked()){
            holder.block.setText("Block");
        }else {
            holder.block.setText("Blocked");
        }

        holder.block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!user.isBlocked()) {
                    user.setBlocked(true);
                    updateUser(user.getId(), user);
                    holder.block.setText("Blocked");
                }else{
                    return;
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        if(uData != null){
            return uData.size();
        }
        return 0;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView username;
        TextView name;
        TextView surname;
        Button block;
        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.userName_aList);
            name = itemView.findViewById(R.id.name_aList);
            surname = itemView.findViewById(R.id.userName_aList);
            block = itemView.findViewById(R.id.block_user);



        }
    }

    private void updateUser(Long id, UserDto userDto){

        UserApiService userApiService = RetrofitClientInstance.getRetrofitInstance(activity).create(UserApiService.class);
        Call<UserDto> call = userApiService.update(id, userDto);
        call.enqueue(new Callback<UserDto>() {
            @Override
            public void onResponse(Call<UserDto> call, Response<UserDto> response) {
                if(response.code() == 200){
                    Toast.makeText(activity, "Sucessfully updated", Toast.LENGTH_SHORT).show();
                    FragmentTransition.to(UsersListFragment.newInstance(),activity,false);
                }
                else if(response.code() == 404){
                    Toast.makeText(activity, "User doesn't exist", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<UserDto> call, Throwable t) {
                Log.i(TAG, t.toString());
            }
        });
    }
}
