package model;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Order {
    @SerializedName("id")
    private Long id;
    @SerializedName("satnica")
    private String time;
    @SerializedName("dostavljeno")
    private boolean delivered;
    @SerializedName("ocena")
    private int rate;
    @SerializedName("komentar")
    private String comment;
    @SerializedName("anonimanKomentar")
    private boolean anonymousComment;
    @SerializedName("arhiviranKomentar")
    private boolean archivedComment;
    @SerializedName("kupac")
    private CustomerDto customer;
    @SerializedName("stavke")
    private List<OrderItem> orderItems = new ArrayList<>();

    public Order() {
    }

    public Order(Long id, String time, boolean delivered, int rate, String comment, boolean anonymousComment, boolean archivedComment, List<OrderItem> orderItems) {
        this.id = id;
        this.time = time;
        this.delivered = delivered;
        this.rate = rate;
        this.comment = comment;
        this.anonymousComment = anonymousComment;
        this.archivedComment = archivedComment;
        this.orderItems = orderItems;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isAnonymousComment() {
        return anonymousComment;
    }

    public void setAnonymousComment(boolean anonymousComment) {
        this.anonymousComment = anonymousComment;
    }

    public boolean isArchivedComment() {
        return archivedComment;
    }

    public void setArchivedComment(boolean archivedComment) {
        this.archivedComment = archivedComment;
    }

    public CustomerDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDto customer) {
        this.customer = customer;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
}
