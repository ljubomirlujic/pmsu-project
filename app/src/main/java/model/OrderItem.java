package model;

import com.google.gson.annotations.SerializedName;

public class OrderItem {
    @SerializedName("id")
    private Long id;
    @SerializedName("kolicina")
    private int quantity;
    @SerializedName("artikal")
    private Article article;

    public OrderItem() {
    }

    public OrderItem(Long id, int quantity, Article article) {
        this.id = id;
        this.quantity = quantity;
        this.article = article;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
