package model;

import com.google.gson.annotations.SerializedName;

public class Customer {
    @SerializedName("adresa")
    private String adress;
    @SerializedName("korisnik")
    private User user;

    public Customer() {
    }

    public Customer(String adress, User user) {
        this.adress = adress;
        this.user = user;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
