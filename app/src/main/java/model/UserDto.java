package model;

import com.google.gson.annotations.SerializedName;

public class UserDto {
    @SerializedName("id")
    private Long id;
    @SerializedName("ime")
    private String name;
    @SerializedName("prezime")
    private String surname;
    @SerializedName("username")
    private String username;

    @SerializedName("blokiran")
    private boolean blocked;

    public UserDto() {
    }

    public UserDto(Long id, String name, String surname, String username, boolean blocked) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.blocked = blocked;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }
}
