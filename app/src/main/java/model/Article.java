package model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Article implements Parcelable {
    @SerializedName("id")
    private Long id;
    @SerializedName("putanjaSlike")
    private String image;
    @SerializedName("naziv")
    private String name;
    @SerializedName("opis")
    private String description;
    @SerializedName("cena")
    private double price;
    @SerializedName("prodavacId")
    private Long sellerId;

    public Article() {
    }

    public Article(String image, String name, String description, double price, Long sellerId) {
        this.image = image;
        this.name = name;
        this.description = description;
        this.price = price;
        this.sellerId = sellerId;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getSeller() {
        return sellerId;
    }

    public void setSeller(Long seller) {
        this.sellerId = seller;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", image='" + image + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", sellerId=" + sellerId +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
