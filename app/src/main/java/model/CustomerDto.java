package model;

import com.google.gson.annotations.SerializedName;

public class CustomerDto {

    @SerializedName("adresa")
    private String adress;
    @SerializedName("korisnik")
    private UserDto user;

    public CustomerDto() {
    }

    public CustomerDto(String adress, UserDto user) {
        this.adress = adress;
        this.user = user;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}

