package model;

import com.google.gson.annotations.SerializedName;

public class SellerDto {

    @SerializedName("email")
    private String email;
    @SerializedName("adresa")
    private String adress;
    @SerializedName("naziv")
    private String companyName;
    @SerializedName("prosjecnaOcjena")
    private double avgRate;
    @SerializedName("korisnik")
    private UserDto user;

    public SellerDto() {
    }

    public SellerDto(String email, String adress, String companyName, UserDto user) {
        this.email = email;
        this.adress = adress;
        this.companyName = companyName;
        this.user = user;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public double getAvgRate() {
        return avgRate;
    }

    public void setAvgRate(double avgRate) {
        this.avgRate = avgRate;
    }
}
