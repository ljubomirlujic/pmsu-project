package model;

import java.time.LocalDate;

public class Discount {
    private Article article;
    private float percent;
    private LocalDate startDate;
    private LocalDate endDate;

    public Discount() {
    }

    public Discount(Article article, float percent, LocalDate startDate, LocalDate endDate) {
        this.article = article;
        this.percent = percent;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Discount{" +
                "article=" + article +
                ", percent=" + percent +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
