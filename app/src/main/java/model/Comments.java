package model;

public class Comments {
    private String name;
    private String lastname;
    private String description;
    private int rate;

    public Comments(){

    }

    public Comments(String name, String lastname, String description, int rate) {
        this.name = name;
        this.lastname = lastname;
        this.description = description;
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return  "name: " + name + "\n"  +
                "lastname: " + lastname + "\n"  +
                "rate: " + rate + "\n"  +
                "description: " + description + "\n" ;
    }
}
