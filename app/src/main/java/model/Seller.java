package model;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDate;

public class Seller {
    @SerializedName("email")
    private String email;
    @SerializedName("adresa")
    private String adress;
    @SerializedName("naziv")
    private String companyName;
    @SerializedName("korisnik")
    private User user;

    public Seller() {
    }

    public Seller(String email, String adress, String companyName, User user) {
        this.email = email;
        this.adress = adress;
        this.companyName = companyName;
        this.user = user;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
